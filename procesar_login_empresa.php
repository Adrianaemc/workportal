<?php
require_once "C:/xampp/htdocs/sitioweb/bd.php"; 

// Iniciar la sesión
session_start();

// Verifica si se han enviado los datos del formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtiene los datos del formulario
    $email = $_POST["email"];
    $contrasena = $_POST["contrasena"];

    // Prepara la consulta SQL para evitar inyección de SQL
    $stmt = $conexion->prepare("SELECT * FROM empresas WHERE correo_electronico=? AND contrasena=?");
    $stmt->bindParam(1, $email);
    $stmt->bindParam(2, $contrasena);

    // Ejecuta la consulta
    $stmt->execute();

    // Obtiene el resultado de la consulta
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    // Verifica si se encontró un registro coincidente
    if ($result) {
        // Guardar detalles de la empresa en la sesión
        $_SESSION['empresa'] = $result;
        
        // Redirigir al perfil de la empresa
        header("Location: perfil_empresa.php"); 
        exit();
    } else {
        // Si las credenciales son incorrectas, almacenar mensaje de error en la sesión
        $_SESSION['mensaje_error'] = "Correo electrónico o contraseña incorrectos. Intente nuevamente.";
        
        // Redirigir a la página de inicio de sesión
        header("Location: login_empresa.php");
        exit();
    }
} else {
    // Si alguien intenta acceder directamente a este archivo, redirige de vuelta al formulario de inicio de sesión
    header("Location: login_empresa.php");
    exit();
}
?>
