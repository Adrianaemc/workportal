<?php
require_once "C:/xampp/htdocs/sitioweb/bd.php"; 

// Iniciar la sesión
session_start();

// Verificar si se han enviado los datos del formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Obtener los datos del formulario
    $email = $_POST["email"];
    $contrasena = $_POST["contrasena"];

    // Preparar la consulta SQL para evitar inyección de SQL
    $stmt = $conexion->prepare("SELECT * FROM usuarios WHERE correo_electronico=? AND contrasena=?");
    $stmt->bindParam(1, $email);
    $stmt->bindParam(2, $contrasena);

    $stmt->execute();

    // Obtener el resultado de la consulta
    $result = $stmt->fetch(PDO::FETCH_ASSOC);

    // Verificar si se encontró un registro coincidente
    if ($result) {
        // Iniciar sesión y guardar detalles del usuario en la sesión
        session_start();
        $_SESSION['usuario'] = $result;
        
        // Redirigir al perfil del usuario
        header("Location: perfil_postulante.php");
        exit(); 
    } else {
        // Si las credenciales son incorrectas, almacenar mensaje de error en la sesión
        $_SESSION['mensaje_error'] = "Correo electrónico o contraseña incorrectos. Intente nuevamente.";
        
        // Redirigir a la página de inicio de sesión
        header("Location: login_postulante.php");
        exit();
    }
} else {
    // Si alguien intenta acceder directamente a este archivo, redirige de vuelta al formulario de inicio de sesión
    header("Location: login_postulante.php");
    exit();
}
?>